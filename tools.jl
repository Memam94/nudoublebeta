
function basestart(wfs::ArrayOfRDWaveform)       
    #get the distribution of baseline starting point 
    basestart_list = []
    for i in range length(wfs)
       wf = wfs.waveform[i]
        append!(basestart_list, wf.value[1])
    end
    uplim = mean(basestart_list) + 1000
    dolim = mean(basestart_list) - 1000
    return uplim, dolim
end    

#-----------------------------------------------------------------
function risepoint(wf::RDWaveform)                  
    #define the index of wf rising point
    base_start = wf.value[1] 
    risepoint_index = 0
    for i in 1:1950
        if wf.value[i] < base_start + 5
            risepoint_index = i
        end
    end
    return risepoint_index
end
#-----------------------------------------------------------------
function tail_slope(wf::RDWaveform, peak, n)     
    #slope of the tail
    peak = findmax(wf.value)[2]
    charge = wf.value[peak:peak+n]
    time = wf.time[peak:peak+n]
    #------------------------------Linear fit of charge and time
    return slope
end

#-----------------------------------------------------------------
function base_slope(wf::RDWaveform, peak, n)   
    #slope of the baseline
    peak = findmax(wf.value)[2]
    charge = wf.value[begin:peak-n]
    time = wf.time[begin:peak-n]
    #------------------------------Linear fit of charge and time
    return slope
end

#-----------------------------------------------------------------
function stats(sample, accepted_list, tau_list)    
    #stats
    println("No. of waveforms =", sample) 
    println("Accepted =", size(accepted_list)) 
    println("mean std = ", mean(stdlist))                                  #means std
    println("max tau = ", -1/maximum(tau_list)*16/1000 , "microsecond")    #max tau
    println("min tau = ", -1/minimum(tau_list)*16/1000 , "microsecond")    #min tau

#-----------------------------------------------------------------

function selection_cut(wfs::ArrayOfRDWaveform)
    #Main
    sample = length(wfs)
    std_list = []               # store std for every wf
    tau_list = []               # store slope of every wf tail
    accepted_list = []          # count the accepted wf
    for i in range sample
        y = wfs.value[i]
        peak = findmax(y.value)[2]
        slope_t1 = tail_slope(y, peak, 500)
        slope_t2 = tail_slope(y, peak, 2000)
        slope_b  = base_slope(y, peak, 100)
        base_start = y.value[1]
        baseline = y[begin:risepoint(y)]
        if (base_start < uplim && base_start > dolim) && (y.value[1000] > 1000) && (peak < 2100 && peak > 1650) && (slope_t2 - slope_t1 < 0.18) && (base_start - mean(baseline) < 50) && (slope_b < 0.001 && slope_b > -0.001)
            append!(accepted_list, base_start)
            append!(std_list,std(baseline))
            slope_tau = tail_slope(y, peak, 3000)
            append!(tau_list, slopetau)
        end
    end
end

