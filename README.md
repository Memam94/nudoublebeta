Some useful tools for the search of neutrinoless double beta decay at GERDA and LEGEND experiments.

## Basic Dependance

- Julia:

```Plots```
```ArraysOfArrays```, ```StaticArrays```, ```Tables```, ```TypedTables```
```Statistics```, ```Random```, ```Distributions```, ```StatsBase```
```Unitful```
```SolidStateDetectors```

- Python:

```numpy```, ```pandas```, ```tqdm```  
```matplotlib```, ```seaborn```, ```pylab```
```scipy```, ```lmfit```
