import pandas as pd
import h5py
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

#-------------------------------------------------
class Waveform:
    def __init__(self,fname):
        '''
        fname (string): path to the raw h5 file
        '''

        # file object
        f = h5py.File(fname, 'r')
        # in tier1 files, the top-level group is called "raw"
        raw = f['raw']
        # access the group "waveform"
        self.wf = raw['waveform']
        # number of waveforms
        n_wf = len(self.wf['values'])
        print('-----')
        print("Waveform object with {0} waveforms".format(n_wf))
        self.n_samples = len(self.wf['values'][0])
        print("{0} DAQ samples".format(self.n_samples))
        print('-----')
    

    def get_waveform(self, idx):
        '''
        idx (int or list): list of wf indices or single index to extract
        Returns: a (nested) array of waveform values for given index (indices),
            each row correwponding to one waveform
        '''

        # access dataset "values" and convert to nested array
        return self.wf['values'][idx]


    def get_times(self, idxs):
        '''
        idxs (list): list of wf indices to extract
        Returns: nested arrays of time points, each row corresponding to one waveform
        '''

        t = [self.get_t(idx) for idx in idxs]
        return np.array(t)


    def get_t(self, idx):       
        '''
        idx (int): wf index
        Returns: array of time points for given waveform
        '''

        # access t0 values
        t0 = self.wf['t0'][idx]
        # access dt value
        dt = self.wf['dt'][idx]
        # construct time points
        return t0 + np.arange(0,self.n_samples)*dt

#-------------------------------------------------------------------------------

hades_raw = 'tier1_datafile.lh5'
# waveform object
wf = Waveform(hades_raw)

## Single waveform: plot 0th waveform
t = wf.get_t(0)
y = wf.get_waveform(0)

#-------------------------------------------------------------------------------

# Sample filter

def createList(l1, l2): 
    return [item for item in range(l1, l2+1)] 
      
idx = createList(1, 10000)  #sample size

#-------------------------------------------------------------------------------

# waveform example

t = wf.get_times(1)    
y = wf.get_waveform(1) 


fig = plt.figure(figsize=(15,8))
plt.plot(y, color='k')
plt.xlim(0, 3500)
plt.yticks([])
plt.annotate('Leading edge', xy =(1850, 13900), xytext =(1350, 14000), arrowprops = dict(color='r', linewidth=0.1, shrink = 0.01), fontsize=15)
plt.annotate('Baseline', xy =(600, 12900), xytext =(600, 12900), arrowprops = dict(color='b', linewidth=0.1, shrink = 0.01), fontsize=15)
plt.annotate('Decay tail', xy =(2700, 14000), xytext =(2700, 13800), arrowprops = dict(color='g', linewidth=0.1, shrink = 0.01), fontsize=15)
plt.xlabel('Time [ns]\n', size=15)
plt.ylabel('Voltage (charge) [a.u.]\n', size=15)

#-------------------------------------------------------------------------------

# Some Stats

lst = [item[0] for item in y]
lst = np.asarray(lst)

uplim = np.mean(lst)+1000
dolim = np.mean(lst)-1000


fig, axs = plt.subplots(1,3, figsize=(20,6), facecolor='w', edgecolor='k')
sns.histplot(ax=axs[0], data=lst, bins=np.arange(lst.min(), lst.max()+1), log_scale=False, element="step", fill=False, color='b')
axs[0].set_xlabel('Time baseline value')
axs[0].set_ylabel('Frequency')
axs[0].axvline(dolim, color='g', alpha=0.8, label='min')
axs[0].axvline(uplim, color='k', alpha=0.8, label='max')
axs[0].set_xlim(11500,14000)
axs[0].legend()

slopebase, stdbase = [], []
for i in range(len(y)):
    n= 1700
    peak = y[i].argmax()
    p1y = y[i][0:n]
    p1t = t[i][0:n]
    stdbase.append(np.std(p1y))
    slope, intercept = np.polyfit(p1t, p1y, 1)
    slopebase.append(slope)

slopebase = np.asarray(slopebase)
sns.histplot(ax=axs[1], data=slopebase, log_scale=False, element="step", fill=False, color='red')
axs[1].set_xlabel('Baseline Slope')
axs[1].set_ylabel('Frequency')
axs[1].set_xlim(-0.002,0.002)
axs[1].set_xticks([-0.002, -0.001, 0 , 0.001, 0.002])

stdbase = np.asarray(stdbase)
sns.histplot(ax=axs[2], data=stdbase, log_scale=False, element="step", fill=False, color='g')
axs[2].set_xlabel('Baseline Std')
axs[2].set_ylabel('Frequency')
axs[2].set_xlim(15, 25)

#####################################################################################

fig, axs = plt.subplots(1,3, figsize=(20,6), facecolor='w', edgecolor='k')

tail = [item.argmax() for item in y]
tail = np.asarray(tail)
sns.histplot(ax=axs[0], data=tail, bins=np.arange(tail.min(), tail.max()+1), log_scale=False, element="step", fill=False, color='b')
axs[0].set_xlabel('Tail max value index')
axs[0].set_ylabel('Frequency')
axs[0].axvline(2100, color='k', alpha=0.8, label='max')
axs[0].axvline(1650, color='g', alpha=0.8, label='min')
axs[0].set_xlim(1600,2200)
axs[0].legend()

tail1slope, stdtail = [], []
fig = plt.figure(figsize=(15,6))
for i in range(len(y)):
    n = 10
    peak = y[i].argmax()
    if peak < 2100 and peak > 1650:
        p1py = y[i][peak:peak+n]
        p1pt = t[i][peak:peak+n]
        slope, intercept = np.polyfit(p1pt, p1py, 1) 
        tail1slope.append(slope)
        stdtail.append(np.std(p1py))

tail1slope = np.asarray(tail1slope)
sns.histplot(ax=axs[1], data=tail1slope, log_scale=False, element="step", fill=False, color='red')
axs[1].set_xlabel('Tail slope')
axs[1].set_ylabel('Frequency')

stdtail = np.asarray(stdtail)
sns.histplot(ax=axs[2], data=stdtail, log_scale=False, element="step", fill=False, color='g')
axs[2].set_xlabel('Tail std')
axs[2].set_ylabel('Frequency')

#-------------------------------------------------------------------------------

# Some stats (Leading edge, decaying tail)
fig, axs = plt.subplots(2, 2, figsize=(20,10), 
                       gridspec_kw={
                           'width_ratios': [1, 2],
                           'height_ratios': [1, 1]})

#fig, axs = plt.subplots(1,4, figsize=(20,6), facecolor='w', edgecolor='k')

#-----------------------------------------------------------------
#define the leading edge start
def risepoint(arr, tim, base):
    newy = []
    newt = []
    for i in range(1950):
        if arr[i] < base+25:
            newy.append(arr[i])
            newt.append(tim[i])
    return newy, newt

#-----------------------------------------------------------------
stdlist, meanslope = [], []
counter1 = []
baseline_list, Basetime_list = [], []
l = len(y)
sample = 20
#-----------------------------------------------------------------
for i in range(sample):
    base = y[i][0]   
    time = t[i][:l]
    y2 = y[i][:l]
    axs[0][0].plot(time, y2)   #original
    
    n = 500
    peak = y2.argmax()
    p1py = y[i][peak:peak+n]  #tail slpoe 1
    p1pt = t[i][peak:peak+n]
    slope, intercept = np.polyfit(p1pt, p1py, 1)  
    
    n2 = 2000
    p2py = y[i][peak:peak+n2]    #tail slope 2
    p2pt = t[i][peak:peak+n2]
    slope2, intercept2 = np.polyfit(p2pt, p2py, 1)  

    p1y = y[i][0:1800]      #baseline slope   [0,peak - n] 
    p1t = t[i][0:1800]
    slopeba, interceptba = np.polyfit(p1t, p1y, 1)
    
    flaty,  flatt= risepoint(y2, time, base)
    #print(base - np.mean(flaty))

    if (base < uplim and base > dolim and y2[1000] > 1000) & (peak < 2100 and peak > 1650) & \
    (slope2 - slope < 0.18) & (base - np.mean(flaty) < 50) & (slopeba < 0.001 and slopeba > -0.001): #1650
        axs[0][1].plot(flatt,flaty)
        axs[0][1].set_xlabel('Time [ns]')
        axs[0][1].set_ylabel('charge [a.u.]')
        baseline_list.append(flaty)
        Basetime_list.append(flatt)
        axs[1][0].plot(time,y2)
        counter1.append(base)
        stdlist.append(np.std(flaty))
        
        p3py = y2[peak:peak+3000]
        p3pt = time[peak:peak+3000]
        slopetau, intercepttau = np.polyfit(p3pt, p3py, 1)  
        meanslope.append(slopetau)
        axs[1][1].plot(p3pt,p3py)

print("No. of waveforms =", sample) 
print("Accepted =", len(counter1)) 
print("mean std = ", np.mean(stdlist))
print("maximum slope = ", max(meanslope), "------------ Minimum slope = ", min(meanslope))
print("max tau = ", -1/max(meanslope)*16/1000 , 'microsecond')
print("min tau = ", -1/min(meanslope)*16/1000 , 'microsecond')

#-------------------------------------------------------------------------------
